### Hi there 👋 I'm Abhilash Tu
- 🌱 I’m currently learning improving my dynamic programming skills.
- 👯 I’m looking to collaborate on open source.
- 🤔 I’m looking for a study buddy.
- 💬 Ask me about my works or about me.
- 😄 Pronouns: He/Him 🚹
- ⚡ Fun fact: I'm Hyperactive, Curious, Sensitive.
- 🤨 Not so fun fact: Introvert, Overthinking.
<!-- status codes -->
<a align="center" href="https://abhilashtuofficial.github.io">
    <p align="center">
    <img src="https://github-readme-stats.vercel.app/api?username=abhilashtuofficial&show_icons=true&theme=midnight-purple" alt="my github stats" width="420"/>&nbsp;
                <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=abhilashtuofficial&layout=compact&theme=midnight-purple" alt="languages" height="165">
    </p>
</a>

<br/>

## Connect with me:  
<a href="https://github.com/AbhilashTUofficial" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://stackoverflow.com/story/abhilash-tu" target="_blank">
<img src=https://img.shields.io/badge/stackoverflow-%23F28032.svg?&style=for-the-badge&logo=stackoverflow&logoColor=white alt=stackoverflow style="margin-bottom: 5px;" />
</a>
<a href="https://www.linkedin.com/in/abhilash-tu-160630190/" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>
<a href="https://medium.com/@abhilash-tu" target="_blank">
<img src=https://img.shields.io/badge/medium-%23292929.svg?&style=for-the-badge&logo=medium&logoColor=white alt=medium style="margin-bottom: 5px;" />
</a>
<a href="https://dev.to/abhilashtuofficial" target="_blank">
<img src=https://img.shields.io/badge/dev.to-%2308090A.svg?&style=for-the-badge&logo=dev.to&logoColor=white alt=devto style="margin-bottom: 5px;" />
</a>
<a href="https://codepen.io/abhilash-tu" target="_blank">
<img src=https://img.shields.io/badge/codepen-%23131417.svg?&style=for-the-badge&logo=codepen&logoColor=white alt=codepen style="margin-bottom: 5px;" />
</a>
<a href="https://twitter.com/Abhilash_TU" target="_blank">
<img src=https://img.shields.io/badge/twitter-%2300acee.svg?&style=for-the-badge&logo=twitter&logoColor=white alt=twitter style="margin-bottom: 5px;" />
</a>
<a href="https://dribbble.com/Abhilash_Tu" target="_blank">
<img src=https://img.shields.io/badge/dribbble-%23E45285.svg?&style=for-the-badge&logo=dribbble&logoColor=white alt=dribbble style="margin-bottom: 5px;" />
</a>
<a href="https://www.facebook.com/Abhilashtuofficial" target="_blank">
<img src=https://img.shields.io/badge/facebook-%232E87FB.svg?&style=for-the-badge&logo=facebook&logoColor=white alt=facebook style="margin-bottom: 5px;" />
</a>
<a href="https://www.instagram.com/abhilash_tu/" target="_blank">
<img src=https://img.shields.io/badge/instagram-%23000000.svg?&style=for-the-badge&logo=instagram&logoColor=white alt=instagram style="margin-bottom: 5px;" />
</a>
<a href="https://www.youtube.com/channel/UC8iP2LKB-V1g2jMTbe6Pb4Q" target="_blank">
<img src=https://img.shields.io/badge/youtube-%23EE4831.svg?&style=for-the-badge&logo=youtube&logoColor=white alt=youtube style="margin-bottom: 5px;" />
</a>
<a href="https://www.behance.net/abhilashstorm" target="_blank">
<img src=https://img.shields.io/badge/behance-%23191919.svg?&style=for-the-badge&logo=behance&logoColor=white alt=behance style="margin-bottom: 5px;" />
</a>   
<a href="https://discord.com/" target="_blank">
<img src=https://img.shields.io/badge/discord-%23191919.svg?&style=for-the-badge&logo=discord&logoColor=white alt=behance style="margin-bottom: 5px;" />
</a>   
<br/>

## Languages and Tools:

[<img align="left" alt="Visual Studio Code" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png">][vscode]
[<img align="left" alt="Visual Studio" width="38px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/visualstudio.png">][vscode]
[<img align="left" alt="Android studio" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/androidstudio.png">][android]
[<img align="left" alt="Intellij" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/intellij.png">][java]
[<img align="left" alt="Pycharm" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/pycharm.png">][python]
[<img align="left" alt="Webstorm" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/webstorm.png">][web]
[<img align="left" alt="Blender" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/blender.png">][behance]
[<img align="left" alt="Adobe xd" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/xd.png">][behance]
[<img align="left" alt="Photoshop" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/ps.png">][behance]

[<img align="left" alt="Git" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/git.png">][github]
[<img align="left" alt="Terminal" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/terminal/terminal.png">][github]
<br/><br/>
[<img align="left" alt="HTML5" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/html/html.png">][web]
[<img align="left" alt="CSS3" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png">][web]
[<img align="left" alt="JavaScript" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png">][js]
[<img align="left" alt="python" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/python.png">][python]
[<img align="left" alt="Dart" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/dart.png">][dart]
[<img align="left" alt="C" width="32px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/c.png">][c/c++]
[<img align="left" alt="C++" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/cpp.png">][c/c++]
[<img align="left" alt="Flutter" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/flutter.png">][flutter]
[<img align="left" alt="Java" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/java.png">][java]
[<img align="left" alt="Shell" width="26px" src="https://github.com/AbhilashTUofficial/AbhilashTuofficial/blob/main/logos/tools/shell.png">][shell]

<br/><br/>

## Last commits:

![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTuofficial/cpp-programming?color=blue&label=c%2B%2B%20programming&logo=c%2B%2B&style=for-the-badge)
![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTuofficial/java-programming?color=orange&label=java%20programming&logo=java&style=for-the-badge)
<br/>
![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTuofficial/javaScript-programming?color=yellow&label=javaScript%20programming&logo=javascript&style=for-the-badge)
![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTuofficial/python-programming?color=blue&label=python%20programming&logo=python&style=for-the-badge)
<br/>
![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTUofficial/Flutter-programming?color=blue&label=Flutter%20programming&logo=flutter&logoColor=lightblue&style=for-the-badge)
![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTUofficial/Dynamic-programming?color=purple&label=Dynamic%20programming&logo=convertio&logoColor=white&style=for-the-badge)
<br/>
![GitHub last commit](https://img.shields.io/github/last-commit/AbhilashTUofficial/Shell-scripting?label=Shell%20scripting&logo=gnu%20bash&style=for-the-badge)


[website]: https://abhilashtuofficial.github.io/
[youtube]: https://www.youtube.com/channel/UC8iP2LKB-V1g2jMTbe6Pb4Q
[instagram]: https://www.instagram.com/abhilash_tu/
[linkdein]: https://www.linkedin.com/in/abhilash-tu-160630190/
[vscode]: https://code.visualstudio.com/
[github]: https://github.com/AbhilashTUofficial
[web]: https://github.com/AbhilashTUofficial/Web-development
[js]: https://github.com/AbhilashTUofficial/JavaScript-programming
[python]: https://github.com/AbhilashTUofficial/Python-programming
[dart]: https://github.com/AbhilashTUofficial/CloneApps
[c/c++]: https://github.com/AbhilashTUofficial/Cpp-programming
[flutter]: https://github.com/AbhilashTUofficial/CloneApps
[java]: https://github.com/AbhilashTUofficial/java-programming
[android]: https://github.com/AbhilashTUofficial/CloneApps
[behance]: https://www.behance.net/abhilashstorm
[shell]: https://github.com/AbhilashTUofficial/Shell-scripting
<br/><br/>
![](https://komarev.com/ghpvc/?username=AbhilashTUofficial)
