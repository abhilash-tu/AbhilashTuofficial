const themes = {
  "midnight-purple": {
    title_color: "9745f5",
    icon_color: "9f4bff",
    text_color: "ffff00",
    bg_color: "000000",
  },
};

module.exports = themes;
